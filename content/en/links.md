---
title: Important Links
description: Important Grey Software Links and Docs
category: Info
position: 1
---

## Team Board

For team members to look at their current issues.

<cta-button link="https://gitlab.com/groups/grey-software/-/boards/3080709" text="View Board"></cta-button>

## Current Milestones Issue Lifecycle Board

For team members to see the organization's issues for the current milestones, categorized by which lifecycle stage the
issue is in.

<cta-button link="https://gitlab.com/groups/grey-software/-/boards/2818019?milestone_title=%23started" text="View Board"></cta-button>

## Current Milestones Priority Board

For team members to see the organization's issues for the current milestones, categorized by priority.

<cta-button link="https://gitlab.com/groups/grey-software/-/boards/3353714?milestone_title=%23started" text="View Board"></cta-button>

## Gitlab Guide

We use as our Gitlab primary collaboration environment, and we encourage team members to study and refer to the helpful
Gitlab guide we've written.

<cta-button link="https://resources.grey.software/gitlab-guide" text="View Guide"></cta-button>

## Sharing Resources

We encourage team members to share useful resources with the global community of software learners.

<cta-button link="https://resources.grey.software/" text="Resources Website"></cta-button>
<cta-button link="https://resources.grey.software/contributing" text="Contributing Guide"></cta-button>
